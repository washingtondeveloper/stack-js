const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const routers = require("./routes");

mongoose.connect(
  "mongodb+srv://washington:washington@cluster0-cpi1k.mongodb.net/omnistack?retryWrites=true&w=majority",
  {
    useNewUrlParser: true
  }
);

const app = express();

app.use(cors());
app.use(express.json());
app.use(routers);

app.listen(3003, () => console.log("Server running on port 3003"));
