const express = require("express");

const DevsController = require("./controllers/DevsController");
const LikeController = require("./controllers/LikeController");
const DislikeController = require("./controllers/DislikeController");

const router = express.Router();

router.get("/devs", DevsController.index);
router.post("/devs", DevsController.store);

router.post("/devs/:devId/likes", LikeController.store);
router.post("/devs/:devId/dislikes", DislikeController.store);

module.exports = router;
