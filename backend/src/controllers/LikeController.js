const Dev = require("../models/Dev");

class LikeController {
  async store(req, res) {
    const { user } = req.headers;
    const { devId } = req.params;

    const loggedDev = await Dev.findById(user);
    const targetDev = await Dev.findById(devId);

    if (!targetDev) {
      return res.status(400).json({ error: "Dev not found" });
    }

    if (targetDev.likes.includes(loggedDev._id)) {
      console.log("Match!");
    }

    loggedDev.likes.push(targetDev._id);

    const devSave = await loggedDev.save();

    res.json(devSave);
  }
}

module.exports = new LikeController();
