import React, { useState } from 'react';

import './Login.css';
import logo from '../assets/logo.svg';
import api from '../services/api';

function Login({ history }) {
	const [ username, setUsername ] = useState('');

	async function onSubmitHandler(e) {
		e.preventDefault();

		const response = await api.post('/devs', { username });

		const { _id } = response.data;

		history.push(`/dev/${_id}`);
	}

	return (
		<div className="login-container">
			<form onSubmit={onSubmitHandler}>
				<img src={logo} alt="Tindev" />
				<input
					placeholder="Digite seu usuario do Github"
					value={username}
					onChange={(e) => setUsername(e.target.value)}
				/>
				<button type="submit">Enviar</button>
			</form>
		</div>
	);
}

export default Login;
